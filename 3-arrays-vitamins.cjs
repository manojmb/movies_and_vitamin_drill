const items = [{
    name: 'Orange',
    available: true,
    contains: "Vitamin C",
}, {
    name: 'Mango',
    available: true,
    contains: "Vitamin K, Vitamin C",
}, {
    name: 'Pineapple',
    available: true,
    contains: "Vitamin A",
}, {
    name: 'Raspberry',
    available: false,
    contains: "Vitamin B, Vitamin A",

}, {
    name: 'Grapes',
    contains: "Vitamin D",
    available: false,
}];


/*

    1. Get all items that are available 
    2. Get all items containing only Vitamin C.
    3. Get all items containing Vitamin A.
    4. Group items based on the Vitamins that they contain in the following format:
        {
            "Vitamin C": ["Orange", "Mango"],
            "Vitamin K": ["Mango"],
        }
        
        and so on for all items and all Vitamins.
    5. Sort items based on number of Vitamins they contain.

    NOTE: Do not change the name of this file

*/


// 1. Get all items that are available 
const availableItems = items.filter(({ available }) => available === true);
console.log("All items that are available are:-");
console.log(availableItems);




// 2. Get all items containing only Vitamin C.
const itemsContaingOnlyVitaminC = items.filter(({ contains }) => contains === "Vitamin C");
console.log("\nItems Containing only vitamin C", itemsContaingOnlyVitaminC);



// 3. Get all items containing Vitamin A.
const itemsContaingVitaminA = items.filter(({ contains }) => contains.includes("Vitamin A"));
console.log("\nItems Containing vitamin A", itemsContaingVitaminA);




// 4. Group items based on the Vitamins that they contain in the following format:
//         {
//             "Vitamin C": ["Orange", "Mango"],
//             "Vitamin K": ["Mango"],
//         }

//         and so on for all items and all Vitamins.
const groupedItem = items.reduce((groupedVitamins, { name, contains }) => {
    const vitamins = contains.split(", ");
    vitamins.forEach((vitamin) => {
        if (groupedVitamins[vitamin] === undefined) {
            groupedVitamins[vitamin] = [name];
        } else {
            groupedVitamins[vitamin].push(name);
        }
    })

    return groupedVitamins;

}, {});
console.log("\nItems grouped based on vitamins", groupedItem);



// 5. Sort items based on number of Vitamins they contain.
const sortedItems = items.sort((itemA, itemB) => {
    return itemA.contains.split(', ').length - itemB.contains.split(', ').length;
})
console.log("\nItems sorted basesd on vitamins they contain:-");
console.log(sortedItems);
