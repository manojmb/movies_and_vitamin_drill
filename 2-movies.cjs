const favouritesMovies = {
    "Matrix": {
        imdbRating: 8.3,
        actors: ["Keanu Reeves", "Carrie-Anniee"],
        oscarNominations: 2,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$680M"
    },
    "FightClub": {
        imdbRating: 8.8,
        actors: ["Edward Norton", "Brad Pitt"],
        oscarNominations: 6,
        genre: ["thriller", "drama"],
        totalEarnings: "$350M"
    },
    "Inception": {
        imdbRating: 8.3,
        actors: ["Tom Hardy", "Leonardo Dicaprio"],
        oscarNominations: 12,
        genre: ["sci-fi", "adventure"],
        totalEarnings: "$870M"
    },
    "The Dark Knight": {
        imdbRating: 8.9,
        actors: ["Christian Bale", "Heath Ledger"],
        oscarNominations: 12,
        genre: ["thriller"],
        totalEarnings: "$744M"
    },
    "Pulp Fiction": {
        imdbRating: 8.3,
        actors: ["Sameul L. Jackson", "Bruce Willis"],
        oscarNominations: 7,
        genre: ["drama", "crime"],
        totalEarnings: "$455M"
    },
    "Titanic": {
        imdbRating: 8.3,
        actors: ["Leonardo Dicaprio", "Kate Winslet"],
        oscarNominations: 13,
        genre: ["drama"],
        totalEarnings: "$800M"
    }
}


/*
    NOTE: For all questions, the returned data must contain all the movie information including its name.

    Q1. Find all the movies with total earnings more than $500M. 
    Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
    Q.3 Find all movies of the actor "Leonardo Dicaprio".
    Q.4 Sort movies (based on IMDB rating)
        if IMDB ratings are same, compare totalEarning as the secondary metric.
    Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
        drama > sci-fi > adventure > thriller > crime

    NOTE: Do not change the name of this file
*/

// Q1. Find all the movies with total earnings more than $500M.
const moviesOver500M = Object.entries(favouritesMovies).filter(([movieName, movie]) => {
    // const earningsNumeric = parseInt(movie.totalEarnings.replace(/\D/g, ''), 10);
    const earningsNumeric = parseInt(movie.totalEarnings.slice(1, -1));
    return earningsNumeric > 500;
});
console.log("Movies earning over $500 are:")
console.log(moviesOver500M);


// Q2. Find all the movies who got more than 3 oscarNominations and also totalEarning are more than $500M.
const moreThan3OscarNominations = Object.entries(favouritesMovies).filter(([movieName, movie]) => {
    const earningsNumeric = parseInt(movie.totalEarnings.replace(/\D/g, ''), 10);
    return earningsNumeric > 500 && movie.oscarNominations > 3;
});
console.log("\nMovies having more than 3 oscar nomination and earning more than 500", moreThan3OscarNominations);


// Q.3 Find all movies of the actor "Leonardo Dicaprio".
const movieOfLeonardoDicaprio = Object.entries(favouritesMovies).filter(([movieName, movie]) => {
    return movie.actors.includes("Leonardo Dicaprio");
});
console.log("\nMovies of Leonardo Dicaprio are: ", movieOfLeonardoDicaprio);



// Q.4 Sort movies (based on IMDB rating)
// if IMDB ratings are same, compare totalEarning as the secondary metric.
sortedIMDBratings = Object.entries(favouritesMovies).sort(([movieNameA, movieA], [movieNameB, movieB]) => {
    const imdbRatings = movieA.imdbRating - movieB.imdbRating;
    const earningsNumericA = parseInt(movieA.totalEarnings.replace(/\D/g, ''), 10);
    const earningsNumericB = parseInt(movieB.totalEarnings.replace(/\D/g, ''), 10);
    const earningsNumeric = earningsNumericA - earningsNumericB;
    return imdbRatings || earningsNumeric;
})
console.log("\nSorted array based on IMBD rating and their earning", sortedIMDBratings);



// Q.5 Group movies based on genre. Priority of genres in case of multiple genres present are:
// drama > sci-fi > adventure > thriller > crime
const groupedMoviesByGenre = Object.keys(favouritesMovies).reduce((movieGroup, currentMovie) => {
    if (favouritesMovies[currentMovie].genre.includes('drama')) {
        movieGroup['drama'].push(currentMovie)
    } else if (favouritesMovies[currentMovie].genre.includes('sci-fi')) {
        movieGroup['sci-fi'].push(currentMovie)
    } else if (favouritesMovies[currentMovie].genre.includes('adventure')) {
        movieGroup.adventure.push(currentMovie)
    } else if (favouritesMovies[currentMovie].genre.includes('thriller')) {
        movieGroup.thriller.push(currentMovie)
    } else {
        movieGroup.crime.push(currentMovie)
    }
    return movieGroup
}, { 'drama': [], 'sci-fi': [], 'adventure': [], 'thriller': [], 'crime': [] })

console.log("\nMovies grouped based on their genre", groupedMoviesByGenre);


